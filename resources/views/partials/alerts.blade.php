@if(session('alert'))
    <div class="alert alert-success" role="alert">
        {!! session('alert') !!}
    </div>
@endif


@if($errors->any())
    <div class="alert alert-danger" role="alert">
       <h5>Errors:</h5>
        <ul>
            @foreach($errors->all() as $error)
                <li>{{$error}}</li>
            @endforeach
        </ul>
    </div>
@endif