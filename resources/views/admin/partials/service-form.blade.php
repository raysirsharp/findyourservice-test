<div class="row">
   
    <div class="col-md-5">
        <!-- title -->
        <div class="form-group">
            <label class="input-title">Title:</label>
            <input value="{{ old('title') ?? $service->title }}" class="form-control" name="title">
        </div>
    </div>
    <div class="col-md-12">
        <!-- description -->
        <div class="form-group">
            <label class="input-title">Description:</label>
            <input value="{{ old('description') ?? $service->description }}" class="form-control" name="description">
        </div>
    </div>
    
</div>
    

<div class="row">
   
    <div class="col-lg-6">
        <!-- address -->
        <div class="form-group">
            <label class="input-title">Address:</label>
            <input value="{{ old('address') ?? $service->address }}" class="form-control" name="address">
        </div>
    </div>
    <div class="col-lg-3">
        <!-- address -->
        <div class="form-group">
            <label class="input-title">City:</label>
            <input value="{{ old('city') ?? $service->city }}" class="form-control" name="city">
        </div>
    </div>
    <div class="col-lg-3">
        <!-- address -->
        <div class="form-group">
            <label class="input-title">State:</label>
            <input value="{{ old('state') ?? $service->state }}" class="form-control" name="state">
        </div>
    </div>
    
</div>


<div class="row">
   
    <div class="col-md-4">
        <!-- address -->
        <div class="form-group">
            <label class="input-title">Zip Code:</label>
            <input value="{{ old('zipcode') ?? $service->zipcode }}" class="form-control" name="zipcode">
        </div>
    </div>
    <div class="col-md-4">
        <!-- address -->
        <div class="form-group">
            <label class="input-title">Latitude:</label>
            <input value="{{ old('geo_lat') ?? $service->geo_lat }}" class="form-control" name="geo_lat">
        </div>
    </div>
    <div class="col-md-4">
        <!-- address -->
        <div class="form-group">
            <label class="input-title">Longitude:</label>
            <input value="{{ old('geo_long') ?? $service->geo_long }}" class="form-control" name="geo_long">
        </div>
    </div>
    
</div>