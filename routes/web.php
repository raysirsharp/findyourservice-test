<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Authentication Routes...
$this->get('login', 'Auth\LoginController@showLoginForm')->name('login');
$this->post('login', 'Auth\LoginController@login');
$this->post('logout', 'Auth\LoginController@logout')->name('logout');

//public routes
Route::get('/', 'HomeController@index');
Route::post('/', 'HomeController@search');

//admin routes
Route::group(['middleware' => 'auth'], function() {
    Route::get('/admin', 'AdminController@index');
    Route::get('/admin/create', 'AdminController@create');
    Route::post('/admin/create', 'AdminController@store');
    Route::get('/admin/{service}/edit', 'AdminController@edit');
    Route::put('/admin/{service}', 'AdminController@update');
    Route::delete('/admin/{service}', 'AdminController@destroy');
});