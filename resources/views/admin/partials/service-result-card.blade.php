<div class="card p-3 mt-3">
    <div class="row container">
       {{-- Buttons --}}
       <div class="col-12 text-right">
            <form action="/admin/{{$service->id}}" method="post">
                @csrf
                @method('DELETE')
                <a class="btn btn-sm btn-primary" href="/admin/{{$service->id}}/edit" role="button"><i class="fa fa-pencil"></i> Edit</a>
                <button class="btn btn-sm btn-danger" type="submit"><i class="fa fa-trash"></i> Delete</button>
            </form>
       </div>
       <div class="col-md-1">
            <strong>ID:</strong><br>
            {{$service->id}}
        </div>
        <div class="col-md-4">
            <strong>Title:</strong><br>
            {{$service->title}}
        </div>
        <div class="col-md-7">
            <strong>Description:</strong><br>
            {{$service->description}}
        </div>
        {{-- HR --}}
        <div class="col-12"><hr></div>
        {{-- /HR --}}
        <div class="col-md-3">
            <strong>Address:</strong><br>
            {{$service->address}}
        </div>
        <div class="col-md-2">
            <strong>City:</strong><br>
            {{$service->city}}
        </div>
        <div class="col-md-2">
            <strong>State:</strong><br>
            {{$service->state}}
        </div>
        <div class="col-md-1">
            <strong>Zip Code:</strong><br>
            {{$service->zipcode}}
        </div>
        <div class="col-md-2">
            <strong>Latitude:</strong><br>
            {{$service->geo_lat}}
        </div>
        <div class="col-md-2">
            <strong>Longitude:</strong><br>
            {{$service->geo_long}}
        </div>
    </div>
</div>