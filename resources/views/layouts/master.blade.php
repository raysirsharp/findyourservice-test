<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title> FindYourService </title>
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<link rel="stylesheet" href="{{ elixir('css/app.css') }}">
   <link rel="shortcut icon" type="image/x-icon" href="{{ asset('favicon.ico') }}">
    @yield('styles')
</head>

	<body>
    @include('partials.alerts')
    @include('partials.admin-nav')
    @include('partials.header')
    
    <div class="app-content" id="app">
        @yield('body')
    </div>
    
    @include('partials.footer')
   
    <script src="{{ elixir('js/app.js') }}"></script>
    @yield('scripts')
</body>

</html>