<header class="container">
    <div class="row">
        
        <!-- Logo -->
        <div class="col-12 pt-4">
            <a href="/">
                <img class="img-fluid" src="{{ asset('img/logo.png') }}" alt="Logo">
            </a>
            <div class="container padding-0-left">
                <i>A simple search lookup tool powered by the latest tech.</i>
            </div>
        </div> 
        
    </div>
</header>

