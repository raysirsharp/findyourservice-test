<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Service;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $services = Service::all();
        
        return view('admin.home')
            ->with('services', $services);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validate
        $request->validate(
            [
                'title' => 'required|max:255',
                'description' => 'required|max:255',
                'address' => 'required|max:255',
                'city' => 'required|max:255',
                'state' => 'required|max:255',
                'zipcode' => 'required|min:5|max:5|regex:/^[0-9]/u',
                'geo_lat' => 'required|max:255',
                'geo_long' => 'required|max:255',
            ]
        );
        
        $service = new Service($request->all());
        
        $service->save();
        
        return redirect('/admin')
            ->with('alert', 'Service successfully created!');
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Service $service)
    {
        return view('admin.edit')
            ->with('service', $service);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Service $service)
    {
        //validate
        $request->validate(
            [
                'title' => 'required|max:255',
                'description' => 'required|max:255',
                'address' => 'required|max:255',
                'city' => 'required|max:255',
                'state' => 'required|max:255',
                'zipcode' => 'required|min:5|max:5|regex:/^[0-9]/u',
                'geo_lat' => 'required|max:255',
                'geo_long' => 'required|max:255',
            ]
        );
        
        
        $service->fill($request->all());
        
        $service->save();
        
        return redirect('/admin')
            ->with('alert', 'Service successfully edited!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Service $service)
    {
        $service->delete();
        
        return redirect('/admin')
            ->with('alert', 'Service successfully deleted!');
    }
}








