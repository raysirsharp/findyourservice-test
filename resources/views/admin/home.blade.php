@extends('layouts.master')


@section('body')
    <div class="row container">
        <div class="col-6">
            <h4><u>Services Offered:</u></h4>
        </div>
        <div class="col-6 text-right">
            <a class="btn btn-sm btn-success" href="/admin/create" role="button"><i class="fa fa-plus"></i> Add a new service</a>
        </div>
    </div>
    
    @if($services->count() < 1)
        @include('admin.partials.no-results')
    @else
        @foreach($services as $service)
            @include('admin.partials.service-result-card')
        @endforeach
    @endif
    
    

@endsection