@auth
    <div class="container auth-container">
        <div class="row">
            <div class="col-12 text-right pt-3">
                <form method="post" action="/logout">
                    @csrf
                    <h4>Admin Navagation:</h4>
                    <a class="btn btn-sm btn-primary" href="/admin" role="button"><i class="fa fa-compass"></i> Directory</a>
                    <button class="btn btn-sm btn-secondary" type="submit"><i class="fa fa-sign-out"></i> Logout</button>
                </form>
            </div>
        </div>
    </div>
@endauth