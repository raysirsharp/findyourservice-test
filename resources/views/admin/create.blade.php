@extends('layouts.master')


@section('body')
    <div class="row container">
        <div class="col-6">
            <h4><u>Create a Service:</u></h4>
        </div>
        <div class="col-6 text-right">
            <a class="btn btn-sm btn-success" href="/admin" role="button"><i class="fa fa-undo"></i> Return to Index</a>
        </div>
    </div>
    <div class="container">
       
        <form method="post" action="/admin/create">
          
            @csrf
           
            @include('admin.partials.service-form', [
                'service' => new App\Service
            ])

            <div class="row padding-10"> 
                <div class="col">
                    <button type="submit" class="btn btn-success">
                        <i class="fa fa-check"></i> Create Service
                    </button>
                </div>
            </div>

        </form>
        
    </div>
    
@endsection