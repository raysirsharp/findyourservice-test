<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Service;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('search');
    }
    
    /**
     * Get results based on search.
     *
     * @return json Response
     */
    public function search(Request $request)
    {
        if (!$request->title) {
            $services = Service::query();
        }
        else {
            $services = Service::where('title', 'LIKE', '%'.$request->title.'%');
        }
            
        //filter results by location
        if ($request->locationFound && $this->validRadius($request->radius) && $request->radius != '-1') { 
            
            //users inputs
            $lat = $request->geoLat;
            $long = $request->geoLong;
            $radius = $request->radius;
            
            $currServices = $services->get();
            
            //query filter
            $services->where( function($query) use ($currServices, $lat, $long, $radius) {
                $flag = false;

                //check all current services inside search radius
                foreach($currServices as $curr) {

                    if ($this->distance($lat, $long, $curr->geo_lat, $curr->geo_long) <= $radius ) {
                        if ($flag == false) {
                            $query->where('id', '=', $curr->id);
                            $flag = true;
                        }
                        else {
                            $query->orWhere('id', '=', $curr->id);
                        }
                    }
                }
                if ($flag == false) $query->where('id', '=', '-1');
            });
        }
        
        return response()->json(['data' => $services->get()]);
    }
    
    /**
     * Helper function - checks valid radius value.
     *
     * @return boolean
     */
    private function validRadius($input) {
        //valid inputs
        $valid_inputs = array('-1','1','2','5','10','25','50','100');
        
        foreach ($valid_inputs as $curr) {
            if ($input == $curr) {
                return true;
            }
        }
        return false;
    }
    
    /**
     * Helper function - calculates distance in KM between 2 sets of coordinates.
     *
     * @return distance
     */
    private function distance($lat1, $lon1, $lat2, $lon2) {
        //exact match
        if (($lat1 == $lat2) && ($lon1 == $lon2)) {
            return 0;
        }
        else {
            $theta = $lon1 - $lon2;
            $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
            $dist = acos($dist);
            $dist = rad2deg($dist);
            $miles = $dist * 60 * 1.1515;

            return $miles * 1.60934;
        }
    }
}









