# Description
FinYourService is a code example made by Ryan Claxton to demo skills using Laravel and Vue.js.
This code allows an admin to enter services within the admin section, the public app allows users to search
these services using geolocations.

# Frameworks and libraries
- Laravel 5.7
- Bootstrap 4
- Font Awesome 4
- Vue.js
- Mix

# Installation
### Into the directory of this project:
- Run `composer install` command
- Run `npm install` command
- Run `npm run dev` command
- Copy and rename `.env.example` to `.env`
- Change `.env` to suite your local/database credentials
- Run `php artisan key:generate`
- Run `php artisan migrate`
- Run `php artisan db:seed` to create an admin

# Admin Section
- Enter `http://{{testdomain}}/login` in your browser

- email: test@gmail.com
- password: secret

# Public Section
- Enter `https://{{testdomain}}` in your browser

# Notes
- Public section requires https for location services to work
- Do not run the db:seed command more than once, it only is meant to create a single test user account
